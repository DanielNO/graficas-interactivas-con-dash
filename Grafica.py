# Modulos de Dash
from flask import Flask, send_from_directory
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State

# Clase para manejar la base de datos
from db import DB

# Modulos de PostgreSQL
#import psycopg2 as pg

# Modulos Pandas
import pandas as pd
import pandas.io.sql as psql

# Modulo para ejecutar javascript
import visdcc

# Modulo para controlar el tiempo
import time
from datetime import datetime

# Modulos del sistema
import os
import shutil
from urllib.parse import quote as urlquote

# Modulo para conseguir el usuario
import getpass

#Instance of DB
database_handler = DB(r'database.ini')

# Obtener las tablas disponibles en la base de datos
table_names = database_handler.get_table_names()

# Nombres de las trazas, fijos para cada ejecución
all_names = [
    "DUT01",
    "DUT02",
    "DUT03",
    "DUT04",
    "DUT05",
    "DUT06",
    "DUT07",
    "DUT08",
    "DUT09",
    "DUT10",
    "DUT11",
    "DUT12",
    "DUT13",
    "DUT14",
    "DUT15",
    "DUT16",
    "DUT17",
    "DUT18",
    "DUT19",
    "DUT20",
    "DUT21",
    "DUT22",
    "DUT23",
    "DUT24",
    "DUT25",
    "DUT26",
    "DUT27",
    "DUT28",
    "DUT29",
    "DUT30",
    "BP",
    "SHR",
    "Vacuum",
    "ColdTrap",
    "BPSP",
    "SHRSP",
    "SHR1",
    "SHR2",
    "SHR3",
    "SHR4"
]

# Comando de javascript para conseguir la posición del cursor
command = '''
        function myFunction(evt) {
            setProps({ 
                'event': {'x':evt.x, 
                          'y':evt.y,
                          'window_w': window.innerWidth}
            })
            console.log(evt)
        }

        document.getElementById("temperature-vs-time").removeEventListener('mousemove', myFunction)
        document.getElementById("temperature-vs-time").addEventListener('mousemove', myFunction)
        console.log(this)
        '''
        
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# Nuestro propio servidor de Flask
server = Flask(__name__)
app = dash.Dash(__name__, external_stylesheets=external_stylesheets, server=server)

# Definicion para las descargas
@server.route("/download/<path:path>")
def download(path):
    """Serve a file from the upload directory."""

    # Usuario actual
    user = getpass.getuser()

    # Directorio de guardado de datos exportados
    SAVE_DIRECTORY = "./Ensayos/{}/".format(user)

    return send_from_directory(SAVE_DIRECTORY, path, as_attachment=True)

# Se declara la forma de la pagina 
app.layout = html.Div([

    # Componente para ejecutar javascript
    visdcc.Run_js(id = 'javascript'),

    # Pestañas de la aplicación
    dcc.Tabs(
        id = 'tabs',
        value = 'graph-tab',
        children = [
            # Pestaña para el gráfico
            dcc.Tab(
                id = 'graph-tab',
                label = u'Gráfica',
                value = 'graph-tab',
                children = [
                    html.Div(id = 'title', children = None),
                    html.Div(
                        id = 'tracking_banner',
                        children = 
                        [
                            html.Div('Tracking:', id = 'tracking_text', className='traking_banner_item'),
                            html.Div(html.Button(
                                'OFF', id='tracking_button'), id='tracking_button_div', className='traking_banner_item'),
                            html.Div(
                                id = 'rs-td',
                                className = 'traking_banner_item',
                                style = {'display': 'none'},
                                children=[
                                dcc.RangeSlider(
                                    id = 'zoom_rangeslider',
                                    min = 0.5,
                                    max = 5,
                                    step = 0.5,
                                    value = [3],  
                                    marks = {
                                        0.5: '0.5 h',
                                        1: '1 h',
                                        1.5: '1.5 h',
                                        2: '2 h',
                                        2.5: '2.5 h',
                                        3: '3 h',
                                        3.5: '3.5 h',
                                        4: '4 h',
                                        4.5: '4.5 h',
                                        5: '5h'
                                    }                          
                                    ),
                                ] 
                                ),
                        ]
                    ),
                    html.Div(
                        id='graphs',
                        children=[
                            dcc.Graph(
                                id = 'temperature-vs-time',
                                config = {                                    
                                    'modeBarButtonsToRemove' : [
                                        'zoom2d', 'pan2d', 'select2d', 'lasso2d', 'zoomIn2d', 'zoomOut2d', 'autoScale2d', 'resetScale2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines'
                                    ]
                                },
                                clear_on_unhover = True
                            ),  
                        
                            dcc.Graph(
                                id = 'rangeslider',
                                config = {                                    
                                    'modeBarButtonsToRemove' : [
                                        'zoom2d', 'pan2d', 'select2d', 'lasso2d', 'zoomIn2d', 'zoomOut2d', 'autoScale2d', 'resetScale2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines'
                                    ]
                                }                    
                        ),]
                    ),
                    # Div para mostrar el hover
                    # El estilo no puede introducirse en el css
                    html.Div(
                        id = 'hover_box',
                        style = {
                            'position': 'absolute',
                            'textAlign': 'left',
                            'background': 'rgba(0, 0, 0, 0)' # Para hacer que el fondo sea transparente
                        }
                    ),
                ],
                className='custom-tab', selected_className='custom-tab--selected',
            ),
            # Pestaña para las opciones
            dcc.Tab(
                id = 'opt-tab',
                label = 'Opciones',
                value = 'opt-tab',
                children = [
                    html.Div([
                        html.H5(['Ensayo a mostrar: '], className = None),

                        html.Div([                        
                            # Componente en el que se elige el ensayo a mostrar
                            dcc.Dropdown(
                                id = 'table_names',
                                options = [{'label': i[0], 'value': i[0]} for i in table_names],
                            )
                        ], className = None)
                    ], className = 'opt_div'),

                    html.Br(),

                    html.Div([
                        html.Div([
                            html.Button(
                                id = 'export_button',
                                n_clicks = 0,
                                children = 'Exportar estado actual',
                                className = 'export_button'
                            ),
                            html.Div(
                                id = 'export_warning',
                                children = None,
                            )
                        ])
                    ], className = 'opt_div export_div')                    
                ],
                className='custom-tab', selected_className='custom-tab--selected',
            )
        ]
    ),

    # Para definir el tiempo de actualización
    dcc.Interval(
        id = 'interval_component',
        interval = 30*1000,  # En milisegundos
    ),

    # Para guardar el zoom y que no se pierda por actualizar extendData
    html.Div(id = 'zoom_state', style = {'display': 'none'}, children = [
                                                                False,  # Zoom state
                                                                0,      # Xaxis range[0]
                                                                0,      # Xaxis range[1]
                                                            ]
    ),

    # Para diferenciar cuando haya cambio en la tabla a leer
    html.Div(id = 'previous_table', style = {'display': 'none'}, children = []),

    # Para saber si se está mostrando el gráfico ampliado
    html.Div(id = 'graph_zoom', style = {'display': 'none'}, children = []),

    # Para saber si se ha creado el grafico
    html.Div(id = 'graph_created', style = {'display': 'none'}, children = False),

    # Para saber si se han cargado los datos
    html.Div(id = 'data_loaded', style = {'display': 'none'}, children = False),
], className = 'main-div')

##########################################################################################################################################################################################
# Callback para ejecución de JavaScript
@app.callback(
    Output('javascript', 'run'),
    [Input('tabs', 'value')]
)
def follow_cursor(tab):
    if tab == 'opt-tab':
        raise dash.exceptions.PreventUpdate
    
    return command

##########################################################################################################################################################################################
# Callback para definición del gráfico
@app.callback(
    [Output('temperature-vs-time', 'figure'),   # main_fig
     Output('tabs', 'value'),
     Output('title', 'children'),
     Output('graph_created', 'children')],
    [Input('table_names', 'value')],
    [State('graph_created', 'children')]
)
def create_graph(table_name, graph_created):

    if table_name == None:
        return dash.no_update, 'opt-tab', dash.no_update, dash.no_update

    database_handler.initialize_db(table_name, all_names)
    names = database_handler.names_in_use[table_name]

    main_fig = {
        'data': [dict(
            x = [],
            y = [],
            mode = 'lines',
            opacity = 1,
            line = {
                'width': 2,
            },
            type = 'scattergl',  
            hoverinfo = 'none',
            name = i
            ) for i in names
        ],
        'layout': dict(
            xaxis = {
                'type': 'date',
                'domain': [0,0.93]
            },
            yaxis = {'title': 'Temperature [ºC]'},
            yaxis2 = {'title': 'Pressure [mbar]', 'overlaying': 'y', 'side': 'right', 
                    'anchor': 'free', 'position': 0.95, 'type': 'log',
                    'showexponent': 'all', 'exponentformat': 'e', 'uirevision': True},
            margin = {'l': 50, 'b': 40, 't': 40, 'r': 10},
            uirevision = True,    # Para que el zoom se mantenga
            hovermode = 'closest',
            animate = True,
            animationDuration = 1000,
            # font = {
            #     'family': "Courier New, monospace"
            # }
            # transition = {
            #     'duration' : 1000,
            #     'easing': 'cubic-in-out'
            # }
        )
    }

    # Los datos con un formato particular, se deben introducir aparte

    # BP
    main_fig['data'][names.index('BP')]['line'] = {
        'width': 3,
        'color': 'blue',
        'dash': 'dash'
    }

    # SHR
    main_fig['data'][names.index('SHR')]['visible'] = 'legendonly'
    main_fig['data'][names.index('SHR')]['line'] = {
        'width': 3,
        'color': 'red',
        'dash': 'dash'
    }

    # Vacuum
    main_fig['data'][names.index('Vacuum')]['line'] = {
        'width': 1,
        'color': 'black'
    }
    main_fig['data'][names.index('Vacuum')]['yaxis'] = 'y2'

    # ColdTrap
    try:
        main_fig['data'][names.index('ColdTrap')]['visible'] = 'legendonly'
    except:
        pass

    # BPSP
    try:
        main_fig['data'][names.index('BPSP')]['visible'] = 'legendonly'
    except:
        pass

    # SHRSP
    try:
        main_fig['data'][names.index('SHRSP')]['visible'] = 'legendonly'
    except:
        pass

    return main_fig, 'graph-tab', table_name, (not graph_created)


##########################################################################################################################################################################################
# Callback para actualización de los datos
@app.callback(
    [Output('temperature-vs-time', 'prependData'),
     Output('temperature-vs-time', 'extendData'),
     Output('tracking_button', 'children'),
     Output('data_loaded', 'children'),
     Output('rs-td', 'style')],
    [Input('interval_component', 'n_intervals'),    # Activación por tiempo
     Input('title', 'children'),                    # Activación por cambio de tabla
     Input('tracking_button', 'n_clicks'),
     Input('zoom_rangeslider', 'value')],                
    [State('tracking_button', 'children'),
     State('rs-td', 'style'),
     State('graph_created', 'children'),
     State('data_loaded', 'children')]
)
def update_graph(n, table_name, nc, time_range, on_off, td_style, graph_created, data_loaded):
    if table_name is None:
        raise PreventUpdate

    prop_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0]

    if prop_id == 'title' or (prop_id == 'tracking_button' and on_off == 'ON') or data_loaded != graph_created:
        df = database_handler.tables[table_name]
        names = database_handler.names_in_use[table_name]

        if database_handler.has_tz:
            index = df.index.strftime('%Y-%m-%d %H:%M:%S').values
        else:
            index = df.index.values

        prependData = (
            {
                'x': [index for i in names],
                'y': [df[i] for i in names]
            }, list(range(len(names))), len(index)
        )

        td_style['display'] = 'none'

        return prependData, None, 'OFF', True, td_style

    elif prop_id == 'tracking_button' or on_off == 'ON' or prop_id == 'zoom_rangeslider':
        interval = time_range[0] * 60 * 6
        df, names = database_handler.return_last(table_name, interval, all_names)

        if database_handler.has_tz:
            index = df.index.strftime('%Y-%m-%d %H:%M:%S').values
        else:
            index = df.index.values

        prependData = (
            {
                'x': [index for i in names],
                'y': [df[i] for i in names]
            }, list(range(len(names))), interval
        )

        td_style['display'] = ''

        if prop_id == 'tracking_button':
            return prependData, None, 'ON', True, td_style

        return prependData, None, dash.no_update, graph_created, td_style

    else:
        try:
            df = database_handler.update_db(table_name, all_names)
            if df.empty:
                del df
                raise PreventUpdate

            names = database_handler.names_in_use[table_name]
        except:
            raise PreventUpdate

        if database_handler.has_tz:
            index = df.index.strftime('%Y-%m-%d %H:%M:%S').values
        else:
            index = df.index.values

        extendData = (
            {
                'x': [index for i in names],
                'y': [df[i] for i in names]
            }, list(range(len(names)))
        )

        return dash.no_update, extendData, dash.no_update, dash.no_update, dash.no_update

    raise PreventUpdate

##########################################################################################################################################################################################
# Callback para control del Hover
@app.callback(
    [Output('hover_box', 'children'),
     Output('hover_box', 'style')],
    [Input('temperature-vs-time', 'hoverData')], 
    [State('javascript', 'event'),
     State('table_names', 'value'),
     State('hover_box', 'style'),
     State('tracking_button', 'children')]
)
def display_hover(hoverData, position, table_name, hover_style, on_off):
    """Función para controlar y mostrar los datos en el hover
    
    Parameters
    ----------
    hoverData : dict
        Diccionario con los valores de hover del usuario
    position : dict
        Diccionario con los valores de posición del cursor y el tamaño de ventana del usuario, provenientes de javascript
    table_name : str
        Nombre de la tabla a mostrar, definida en la base de datos
    hover_style : dict
        Diccionario con los valores actuales del estilo del Div del hover
    
    Returns
    -------
    table : html.Div
        Div que contiene los datos actualizados a mostrar en el hover
    hover_style : dict
        Diccionario que contiene los datos actualizados del estilo del hover, con la posición actual del cursor
    
    Raises
    ------
    dash.exceptions.PreventUpdate
        Parar la ejecución de la función y no devolver ningún resultado
    """

    if table_name is None:
        raise dash.exceptions.PreventUpdate
    if position is None:
        raise dash.exceptions.PreventUpdate
    if hoverData is None:
        return None, hover_style

    try:
        curve_name = database_handler.names_in_use[table_name][hoverData['points'][0]['curveNumber']]

        table = database_handler.calculate_increments(table_name, hoverData['points'][0]['pointNumber'], hoverData['points'][0]['curveNumber'], curve_name, on_off)

        if (position['window_w'] - position['x'])<340:
            hover_style['left'] = position['x'] - 340
        else:
            hover_style['left'] = position['x']

        hover_style['top'] = position['y']

        return table, hover_style
    except:
        raise PreventUpdate

##########################################################################################################################################################################################
# Callback para control del Rangeslider
@app.callback(
    [Output('rangeslider', 'figure'),
     Output('zoom_state', 'children')],
    [Input('interval_component', 'n_intervals'),
     Input('temperature-vs-time', 'relayoutData'),
     Input('table_names', 'value'),
     Input('zoom_rangeslider', 'value')],
    [State('zoom_state', 'children'),
     State('tracking_button', 'children')]
)
def create_rangeslider(n, main_zoom, table_name, time_range, zoom_state, on_off):
    """Función para actualización del gráfico inferior, que sirve como referencia y muestra la zona del 
       gráfico principal en la que se está haciendo zoom
    
    Parameters
    ----------
    n : int
        Número de intervalos desde el inicio. Solo sirve para realizar la llamada de la función
    main_zoom : dict
        Zoom actual de la función, se hace None si se usa extendData    
    table_name : str
        Nombre de la tabla a mostrar, definida en la base de datos
    zoom_state : dict
        Zoom almacenado de la función
    
    Returns
    -------
    slider_fig : dict
        Diccionario que contiene todos los datos y configuración del rangeslider, su forma es:
        {'data': list of dicts; 'layout': dict}
    command : str
        Comando de javascript para conseguir la posición del cursor y el tamaño de la ventana
    zoom_state : dict
        Zoom almacenado de la función
    
    Raises
    ------
    dash.exceptions.PreventUpdate
        Parar la ejecución de la función y no devolver ningún resultado
    """
    if table_name is None:
        raise dash.exceptions.PreventUpdate

    try:
        df = database_handler.tables[table_name]['BP']
    except:
        try:
            time.sleep(0.5)
            df = database_handler.tables[table_name]['BP']
        except:
            raise dash.exceptions.PreventUpdate

    if database_handler.has_tz:
        index = df.index.strftime('%Y-%m-%d %H:%M:%S').values
    else:
        index = df.index.values
    y = df.values

    # Rangeslider
    slider_fig = {
        'data': [dict(
            x = index,
            y = y,
            yaxis = 'y2',
            mode = 'lines',
            opacity = 1,
            line = {
                'width': 3,
                'color': 'blue',
                'dash': 'dash'
            },
            type = 'scattergl',  
            hoverinfo = 'none',
            name = 'ColdTrap'
        )],
        'layout': dict(
            showlegend = True,
            xaxis = {
                'type': 'date',
                'domain': [0,0.925],
                'fixedrange': True,
                'showgrid': False
            },
            yaxis = {
                'fixedrange': True,
                'showticklabels': False,
                'showgrid': False
            },
            yaxis2 = {
                'fixedrange': True,
                'showticklabels': False,
                'showgrid': False,
                'overlaying': 'y', 'side': 'right', 
                'anchor': 'free', 'position': 0.95,
                'showexponent': 'all', 'exponentformat': 'e', 'uirevision': True
            },
            margin = {'l': 50, 'b': 40, 't': 0, 'r': 10},
            hovermode = 'closest',                
            transition = {'duration': 1000}
        ),
    }

    if on_off == 'ON':
        interval = int(time_range[0] * 60 * 6)

        slider_fig['layout']['shapes'] = [
            dict(
                type = 'rect',
                xref = 'x',
                yref = 'paper',
                x0 = index[0],
                y0 = 0,
                x1 = index[interval*-1] if interval <= len(index) else index[0],
                y1 = 1,
                fillcolor = 'Black',
                opacity = 0.5,
                layer = 'below',
                line_width = 0
            )
        ]

        return slider_fig, dash.no_update

    if main_zoom is None:
        if zoom_state[0] is True:
                slider_fig['layout']['shapes'] = [
                dict(
                    type = 'rect',
                    xref = 'x',
                    yref = 'paper',
                    x0 = index[0],
                    y0 = 0,
                    x1 = zoom_state[1],
                    y1 = 1,
                    fillcolor = 'Black',
                    opacity = 0.5,
                    layer = 'below',
                    line_width = 0
                ),
                dict(
                    type = 'rect',
                    xref = 'x',
                    yref = 'paper',
                    x0 = zoom_state[2],
                    y0 = 0,
                    x1 = index[-1],
                    y1 = 1,
                    fillcolor = 'Black',
                    opacity = 0.5,
                    layer = 'below',
                    line_width = 0
                )
            ]
        return slider_fig, dash.no_update

    if 'autosize' in main_zoom or 'xaxis.autorange' in main_zoom:
        slider_fig['layout']['shapes'] = None
        zoom_state[0] = False
    else:
        slider_fig['layout']['shapes'] = [
            dict(
                type = 'rect',
                xref = 'x',
                yref = 'paper',
                x0 = index[0],
                y0 = 0,
                x1 = main_zoom['xaxis.range[0]'],
                y1 = 1,
                fillcolor = 'Black',
                opacity = 0.5,
                layer = 'below',
                line_width = 0
            ),
            dict(
                type = 'rect',
                xref = 'x',
                yref = 'paper',
                x0 = main_zoom['xaxis.range[1]'],
                y0 = 0,
                x1 = index[-1],
                y1 = 1,
                fillcolor = 'Black',
                opacity = 0.5,
                layer = 'below',
                line_width = 0
            )
        ]
        zoom_state = [
            True, 
            main_zoom['xaxis.range[0]'], 
            main_zoom['xaxis.range[1]'], 
        ]

    return slider_fig, zoom_state

##########################################################################################################################################################################################
# Función para buscar los archivos disponibles
def saved_files(SAVE_DIRECTORY):
    """List the files in the save directory."""
    files = []
    for filename in os.listdir(SAVE_DIRECTORY):
        path = os.path.join(SAVE_DIRECTORY, filename)
        if os.path.isfile(path):
            files.append(filename)
    return files

def file_download_link(filename):
    """Create a Plotly Dash 'A' element that downloads a file from the app."""
    location = "/download/{}".format(urlquote(filename))
    return html.A(filename, href=location)

# Callback para exportar datos
@app.callback(
    Output('export_warning', 'children'),
    [Input('export_button', 'n_clicks')],
    [State('table_names', 'value'),
     State('zoom_state', 'children')]
)
def export_actual(n, table_name, main_zoom):
    if n == 0:
        raise dash.exceptions.PreventUpdate

    # Usuario actual
    user = getpass.getuser()

    # Directorio de guardado de datos exportados
    SAVE_DIRECTORY = "./Ensayos/{}".format(user)
    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    path = os.path.join(SAVE_DIRECTORY, '{}_{}.csv'.format(table_name, current_time))
    
    if not os.path.exists(SAVE_DIRECTORY):
        # shutil.rmtree(SAVE_DIRECTORY)
        os.makedirs(SAVE_DIRECTORY)

    if table_name == None:
        return ['No hay ensayo seleccionado']
    
    if not main_zoom[0]:
        database_handler.tables[table_name].to_csv(path, sep = ';')
    else:
        df = database_handler.tables[table_name][database_handler.tables[table_name].index >= main_zoom[1]]
        df = df[df.index <= main_zoom[2]]
        df.to_csv('.\Ensayos\{}\{}_{}.csv'.format(user, table_name, current_time), sep = ';')

    files = saved_files(SAVE_DIRECTORY)

    return [html.Li(file_download_link(filename)) for filename in files]

if __name__ == '__main__':
    app.run_server(debug = True, port=8000, host = '192.168.20.23')
    # app.run_server(debug = True)
