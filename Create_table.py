import psycopg2 as pg
from config import config
import pandas.io.sql as psql

def create_table(table_name):
    """Funcion para crear tablas para ensayos con un nombre variable"""
    
    command = """
            CREATE TABLE "{}"
            (
                "timestamp" varchar(20),
                "DUT01" real,
                "DUT02" real,
                "DUT03" real,
                "DUT04" real,
                "DUT05" real,
                "DUT06" real,
                "DUT07" real,
                "DUT08" real,
                "DUT09" real,
                "DUT10" real,
                "DUT11" real,
                "DUT12" real,
                "DUT13" real,
                "DUT14" real,
                "DUT15" real,
                "DUT16" real,
                "DUT17" real,
                "DUT18" real,
                "DUT19" real,
                "DUT20" real,
                "DUT21" real,
                "DUT22" real,
                "DUT23" real,
                "DUT24" real,
                "DUT25" real,
                "DUT26" real,
                "DUT27" real,
                "DUT28" real,
                "DUT29" real,
                "DUT30" real,
                "BP" real,
                "SHR" real,
                "Vacuum" real,
                "ColdTrap" real,
                "BPSP" real,
                "SHRSP" real
            )
            """.format(table_name)
    
    conn = pg.connect(**config())
    psql.execute(command, conn)
    conn.commit()
    conn.close()

# if __name__ == "__main__":
#     create_table('Ensayo2')