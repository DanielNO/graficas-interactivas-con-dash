from sqlalchemy import create_engine
import pandas as pd
import pandas.io.sql as psql
import pymysql
import psycopg2 as pg
import numpy as np
from configparser import  ConfigParser
import mysql.connector as connector
import threading
import time
import dash
import dash_html_components as html

class DB():
    """
    Clase para controlar los accesos a la base de datos y operaciones del gráfico

    """
    
    def __init__(self,config_file = 'database.ini', index_name = 'timestamp', null = -200):
        self.db_config = self.config(config_file)
        self.schema = self.db_config['database']
        self.conn = None
        self.create_connector()
        self.tables = dict()
        self.index = dict()
        self.index_name = index_name
        self.tables_in_use = set()
        self.names_in_use = dict()
        self.has_tz = False
        self.cleaner_thread = threading.Thread(target=self.cleaner, daemon=True)
        self.null = null # Valor de los datos nulos

    def config(self, filename = 'database.ini'):
        """Función para configurar el acceso a la base de datos
        
        Parameters
        ----------
        filename : str, optional
            Nombre del archivo que contiene los datos de acceso a la base de datos, 
            by default 'database.ini'
        
        Returns
        -------
        db_config : dict
            Diccionario que define la conexión a la base de datos        
        """

        # Create a parser
        parser = ConfigParser()
        # Read config file
        parser.read(filename)

        # Get section, default to postgresql
        db_config = {}
        section = parser.sections()[0]
        #TODO check the structure of the file
        params = parser.items(section)
        db_config['flavor'] = section
        for param in params:
            db_config[param[0]] = param[1]
        
        return db_config

    def create_connector(self):
        """Returns the connector when intancing the class.
        
        Returns:
            [type] -- [description]
        """
        if self.db_config['flavor'] == 'postgresql':
            self.conn = create_engine('postgresql://{}:{}@{}:5432/{}'.format(
                self.db_config['user'],
                self.db_config['password'],
                self.db_config['host'],
                self.db_config['database']))
        elif self.db_config['flavor'] == 'MySQL':
            self.conn = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(
                self.db_config['user'],
                self.db_config['password'],
                self.db_config['host'],
                self.db_config['database']))
        else:
            print('DB flavor not recognized')

    def get_table_names(self):
        """Función para obtener los nombres de las tablas disponibles en la base de datos
        
        Returns
        -------
        pandas.Dataframe
            Dataframe con los nombres de las tablas en la base de datos       
        """
        return psql.read_sql("""SELECT table_name FROM information_schema.tables WHERE table_schema = '{}'""".format(self.schema), self.conn).values.tolist()

    def initialize_db(self,table_name, all_names):
        """función para obtener los valores disponibles en la tabla solicitada
        
        Parameters
        ----------
        table_name : str
            Nombre de la tabla a mostrar, definida en la base de datos
        
        Returns
        -------
        pandas.Dataframe
            Dataframe con todos los datos disponibles en la tabla correspondiente
        """

        if table_name in self.tables_in_use:
            # Actualizar los valores que haya
            df = self.update_db(table_name, all_names)
            del df
        else:
            # Obtener todos los valores actuales de la tabla
            self.tables[table_name] = psql.read_sql('SELECT * FROM {}.{} ORDER BY timestamp ASC'.format(self.schema, table_name), self.conn, self.index_name)

            if self.null == 'NaN':
                self.tables[table_name] = self.tables[table_name].loc[:, (self.tables[table_name].notnull()).all(axis=0)]
            else:
                self.tables[table_name] = self.tables[table_name].loc[:,(self.tables[table_name] != -200.0).all(axis=0)]

            index = self.tables[table_name].index
            if type(index) == pd.core.indexes.base.Index:
                self.index[table_name] = index.values
            else:
                self.index[table_name] = index.strftime('%Y-%m-%d %H:%M:%S').values
                self.has_tz = True

            self.tables_in_use.add(table_name)
            self.names_in_use[table_name] = [x for x in all_names if x in set(self.tables[table_name].columns)]

        # if not self.cleaner_thread.isAlive():
        #     try:
        #         self.cleaner_thread.join() #Kill thread
        #     except:

        #         print("no puedo cerrar")
        #     self.cleaner_thread.start()

    def update_db(self,table_name, all_names):
        """Función para conseguir los últimos datos añadidos a la tabla
        
        Parameters
        ----------
        table_name : str
            Nombre de la tabla a mostrar, definida en la base de datos
        
        Returns
        -------
        df : pandas.Dataframe
            
        """
        df = psql.read_sql(
                        '''SELECT * FROM {}.{} WHERE timestamp > '{}' ORDER BY timestamp ASC'''.format(self.schema, table_name, self.index[table_name][-1]), 
                        self.conn, self.index_name
                    )       
        if self.null == 'NaN':
            df = df.loc[:,(df.notnull()).all(axis=0)]
        else:
            df = df.loc[:,(df != -200.0).all(axis=0)]
            
        if not df.empty:
            if not df.columns.all(self.names_in_use[table_name]):
                self.names_in_use[table_name] = [x for x in all_names if x in set(self.tables[table_name].columns)]
            if self.has_tz:
                index = df.index.strftime('%Y-%m-%d %H:%M:%S').values
                self.index[table_name] = np.append(self.index[table_name], index)
            else:
                index = df.index.values
                self.index[table_name] = np.append(self.index[table_name], index)
            self.tables[table_name] = self.tables[table_name].append(df)

        return df

    def calculate_increments(self, table_name, row, column, curve_name, on_off):
        """Función para calcular los valores de incremento de temperatura y velocidad de cambio
           en 1min, 5min, 30min y 1h para el punto actual
        
        Parameters
        ----------
        table_name : str
            Nombre de la tabla a mostrar, definida en la base de datos
        row : int
            Fila del punto actual
        column : int
            Columna del punto actual
        curve_name : str
            Nombre de la traza a la que pertenece el punto actual
        
        Returns
        -------
        html.Div
            Div que contiene la infomación a mostrar en el hover de Dash
        """

        # El orden del tracking está invertido
        if on_off == 'ON':
            row = len(self.index[table_name]) - row - 1

        initial_value = self.tables[table_name].iloc[row][column]
        point = [self.index[table_name][row], initial_value]

        if curve_name is 'Vacuum':
            return html.Div([
                html.H6("Vacuum"),
                html.H6("{}, {:.2e} mbar".format(point[0], point[1]))
                ], id = 'hover_in')

        increments = [
            (initial_value - self.tables[table_name].iloc[row - 6][column]) if row-6>=0 else 'NaN',
            (initial_value - self.tables[table_name].iloc[row - 30][column]) if row-30>=0 else 'NaN',
            (initial_value - self.tables[table_name].iloc[row - 180][column]) if row-180>=0 else 'NaN',
            (initial_value - self.tables[table_name].iloc[row - 360][column]) if row-360>=0 else 'NaN',
        ]

        velocity = [
            ([increments[0], increments[0] * 60]) if increments[0] != 'NaN' else ['NaN', 'NaN'],
            ([increments[1] / 5, increments[1] * 12]) if increments[1] != 'NaN' else ['NaN', 'NaN'],
            ([increments[2] / 30, increments[2] * 2]) if increments[2] != 'NaN' else ['NaN', 'NaN'],
            ([increments[3] / 60, increments[3]]) if increments[3] != 'NaN' else ['NaN', 'NaN'],
        ]

        row = ['1min', '5min', '30min', '1h']
        
        for i in range(len(increments)):
            if increments[i] != 'NaN':
                increments[i] = round(increments[i], 1)
                velocity[i][0] = round(velocity[i][0], 1)
                velocity[i][1] = round(velocity[i][1], 1)
                row[i] =  html.Tr([
                     html.Td(row[i]), html.Td('{} ºC'.format(increments[i])), html.Td('{} ºC/min'.format(velocity[i][0])), html.Td('{} ºC/h'.format(velocity[i][1]))
                 ])
            else:
                row[i] = html.Tr([html.Td(row[i]), html.Td('NaN'), html.Td('NaN'), html.Td('NaN')])

        return html.Div([
             html.H6("{}".format(curve_name)),
             html.H6("{}, {} ºC".format(point[0], round(point[1],1))),
             html.Table([
                 i for i in row
             ], id = 'hover_table')
             ], id = 'hover_in')

    def return_last(self, table_name, period, all_names):
        df = psql.read_sql('SELECT * FROM {}.{} ORDER BY timestamp DESC LIMIT {}'.format(self.schema, table_name, int(period)), self.conn, self.index_name)
        if self.null == 'NaN':
            df = df.loc[:,(df.notnull()).all(axis=0)]
        else:
            df = df.loc[:,(df != -200.0).all(axis=0)]
        names = [x for x in all_names if x in set(self.tables[table_name].columns)]
        return df, names

    def cleaner(self):
        """
        Clean tables not in use. 
        """
        #Threaded function to clean unused tables

        while True:

            print("hola desde cleaner")
            table_name_list = list(self.tables.keys())

            if self.tables:


                for table_name in table_name_list:
                    if table_name in self.tables_in_use:
                        print(table_name + " is in use")
                    else:
                        print(table_name + " is not in use. Freeing memory")

                        del self.tables[table_name]
            else:
                #No tables
                print("no tables exiting")
                break

            self.tables_in_use = set()

            time.sleep(10)

        pass