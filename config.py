from configparser import  ConfigParser

# Se importa la configuracion desde un archivo
# Es aconsajable establecer la ruta al archivo
def config(filename = 'C:/<pathtofile>/database.ini', section = 'postgresql'):
    # Create a parser
    parser = ConfigParser()
    # Read config file
    parser.read(filename)

    # Get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {} not found in the {} file'.format(section, filename))

    return db