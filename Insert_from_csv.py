# Modulos de Dash
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

# Modulos de PostgreSQL
import psycopg2 as pg
from config import config

# Modulos Pandas
import pandas as pd
import pandas.io.sql as psql

# Modulo de tiempo para controlar el ritmo de actualizacion
import time

# Se establece la conexion con el servidor en base a los 
# parametros establecidos en el archivo "database.ini"
conn = pg.connect(**config())

# Se leen los datos del csv correspondiente
# En este caso de un servidor local
df = pd.read_csv('http://localhost:1337/datos.csv', sep = ';')

# Se obtiene la longitud maxima del DataFrame
rows = len(df['timestamp'])

# Se reinicia la tabla usada para el ensayo
psql.execute('DELETE FROM "Ensayo"', conn)

# Se insertan los valores en la base de datos por filas
for i in range(rows):
    row = df.iloc[i].values.tolist()
    psql.execute('''INSERT INTO "Ensayo"(timestamp, 
                                          "DUT01", "DUT02", "DUT03", "DUT04", "DUT05", "DUT06", "DUT07", "DUT08", "DUT09", "DUT10",
                                          "DUT11", "DUT12", "DUT13", "DUT14", "DUT15", "DUT16", "DUT17", "DUT18", "DUT19", "DUT20",
                                          "DUT21", "DUT22", "DUT23", "DUT24", "DUT25", "DUT26", "DUT27", "DUT28", "DUT29", "DUT30",
                                          "BP", "SHR", "Vacuum", "ColdTrap", "BPSP", "SHRSP") 
                                          VALUES({})
                '''.format(str(row)[1:-1]), conn)
    conn.commit()
    db = psql.read_sql('SELECT * FROM "Ensayo"', conn)
    print(db)
    # time.sleep(5)

conn.close()